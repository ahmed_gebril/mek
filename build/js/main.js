$(document).ready(function () {
  let navSearchForm = $("#navSearchForm")
  let navSearchIcon = $(".mainNav-search_icon")
  navSearchIcon.on("click", function (e) {
    e.preventDefault();
    if (navSearchForm.hasClass("active")) {
      navSearchForm.submit();
    }
    navSearchForm.toggleClass("active");
    if (navSearchForm.hasClass("active")) {
      navSearchForm.find('#navSearch').focus();
    }
  });

  $("body").mouseup(function (e) {
    var container = navSearchForm;
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      navSearchForm.removeClass("active");
    }

    var container2 = $(".mainNav-cart");
    // if (!container.is(e.target) && container.has(e.target).length === 0) {
    //   $(".cartWrapper").removeClass("expanded");
    // }
  });
  $(window).click(function() {
    $(".cartWrapper").removeClass("expanded");
    });

    $('.cartWrapper ,.mainNav-cart_icon').click(function(event){
        event.stopPropagation();
    });
  $('.formInput').on({
    focus: function () {
      $(this).parent().addClass('focused')
    },
    blur: function () {
      if ($(this).val().length < 1) {
        $(this).parent().removeClass('focused')
      }
    }
  });

  $(".heroArea-carousel").each(function () {
    $(this).slick({
      slidesToShow: 1,
      rtl: document.dir === "rtl" ? true : false,
      prevArrow: $(this).next().find('.prev'),
      nextArrow: $(this).next().find('.next'),
    });
  });

  var currency = "EGP";
  var universalCurrency = "EGP";
  function localStringToNumber(s) {
    return Number(String(s).replace(/[^0-9.-]+/g, ""));
  }

  function onFocus(e) {
    var value = e.target.value;
    e.target.value = value ? localStringToNumber(value) : '';
  }

  function onBlur(e) {
    var value = e.target.value;

    const options = {
      maximumFractionDigits: 2,
      currency: currency,
      style: "currency",
      currencyDisplay: "symbol"
    }

    e.target.value = value
      ? localStringToNumber(value).toLocaleString(undefined, options)
      : `${currency} 0.00`
  }
  $('input[type="currency"]').each(function () {
    $(this).attr("placeholder", `0.00 ${currency}`)
  })
  $('input[type="currency"]').on("focus", onFocus);
  $('input[type="currency"]').on("blur", onBlur);

  $(".wrapScroll").niceScroll({
    cursorcolor: "#d8801e",
    cursorwidth: "3px",
    zindex: 1,
    cursorminheight: 32, 
    // cursorfixedheight: "280px",
    background: "#fff",
    cursorborder: "0px solid #fff",
    cursorborderradius: 0,
    autohidemode: true
  });

  $(".mainNav-cart_icon").on('click', function (e) {
    e.preventDefault();
    $(".cartWrapper").toggleClass("expanded");
  });

  // Cause
  $(".causes-carousel").each(function () {
    $(this).slick({
      slidesToShow: 3,
      infinite: false,
      rtl: document.dir === "rtl" ? true : false,
      prevArrow: $(this).next().find('.prev'),
      nextArrow: $(this).next().find('.next'),
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            infinite: false,
      rtl: document.dir === "rtl" ? true : false,
      prevArrow: $(this).next().find('.prev'),
      nextArrow: $(this).next().find('.next'),
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            infinite: false,
      rtl: document.dir === "rtl" ? true : false,
      prevArrow: $(this).next().find('.prev'),
      nextArrow: $(this).next().find('.next'),
          }
        }
      ]
    });
  });
  $(".cause").each(function () {
    let $this = $(this);
    $this.on("submit", function (e) {
      e.preventDefault();
    });
    $(this).find("form > div").not(":first").hide();
    $(this).find(".go-price").on("click", function () {
      $this.find(".cause-price").show().siblings().hide();
      $this.find(".cause-price input").focus();
    });
    $(this).find(".cause-price .causeWide-submit_icon").on("click", function () {
      let value = Number($this.find("input.cause-price_input").val().replace(/[^0-9.-]+/g, ""));
      // Ajax Here
      $this.find(".cause-price_submitted").text($this.find("input.cause-price_input").val())
      $this.find(".cause-submit").show().siblings().hide();
    });
    $(this).find(".edit-price").on("click", function () {
      $this.find(".go-price").click();
    })
  }); 

  
  $(".section-news .slider").each(function () {
    $(this).slick({
      arrows:true,
      slidesToShow: 3,
      infinite: false,
      rtl: document.dir === "rtl" ? true : false,
      prevArrow: $(this).next().find('.prev'),
      nextArrow: $(this).next().find('.next'),
      responsive: [
        
        
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: true,
            infinite: true,
            centerPadding: '20px',
          }
        }
      ]
    });
  });
  $(".scoops.slider").each(function () {
    if($( window ).width() <= 1199){
      $(this).slick({
        slidesToShow: 5,
        infinite: false,
        arrows:false,
        draggable:false,
        rtl: document.dir === "rtl" ? true : false,
        
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
              draggable:true
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              draggable:true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              draggable:true
            }
          },
          {
            breakpoint: 500,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              draggable:true
            }
          }
        ]
      });
    }
    
  });

  $(".campaigns-carousel").each(function () {
    $(this).slick({
      slidesToShow: 1,
      infinite: false,
      adaptiveHeight: true,
      rtl: document.dir === "rtl" ? true : false,
      prevArrow: $(this).next().find('.prev'),
      nextArrow: $(this).next().find('.next'),
    });
  });

  $(".scoop").hover(function () {
    let img = $(this).find(".scoop-figure").clone().addClass("full-width-figure");
    $(this).siblings().css({
      opacity: "0.15"
    });
    $(this).parent().find(".total-services").css({
      opacity: "0"
    });
    $(this).siblings().find(".scoop-desc").css({
      opacity: "0"
    });
    $(this).find(".scoop-figure").css({
      opacity: "0"
    });
    $(this).parent().append(img);
  }, function (params) {
    $(this).siblings().css({
      opacity: "1"
    })
    $(this).parent().find(".total-services").css({
      opacity: "1"
    });
    $(this).siblings().find(".scoop-desc").css({
      opacity: "1"
    })
    $(this).find(".scoop-figure").css({
      opacity: "1"
    });
    $(this).parent().find(".full-width-figure").remove();
  });


  $(".scroll-spy").each(function () {
    $(this).scrollspy({ animate: true, offset: -160 });
  });



  $(".subNav-login").click(function (e) {
    e.preventDefault();

    $(".login-wrapper").css("display", "flex").hide().fadeIn();

    $('html, body').css({
      overflow: 'hidden',
    });
  })
  $(".cause-share").click(function (e) {
    e.preventDefault();

    $(".share-wrapper").css("display", "flex").hide().fadeIn();

    $('html, body').css({
      overflow: 'hidden',
    });
  })

  $(".close-forms, .close-pop").click(function () {
    $(".login-wrapper, .share-wrapper, .pop-wrapper").fadeOut();
    $('html, body').css({
      overflow: 'auto',
    });
    $(".login-wrapper, .share-wrapper, .pop-wrapper").find("input").val("");
  });

  $(".signup-cta").click(function (e) {
    e.preventDefault();
    $("#loginForm").hide()
    $("#signupForm").fadeIn()
  });

  $(".login-cta").click(function (e) {
    e.preventDefault();
    $("#signupForm").hide()
    $("#loginForm").fadeIn()
  });

  $(".login-wrapper, .pop-wrapper ,.share-wrapper").mouseup(function (e) {
    var container = $(".form-wrapper");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $(".close-forms, .close-pop").click();
    }
  });
  if ($('.slider-cont').length) {
    $('.slider-cont').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      rtl: document.dir === "rtl" ? true : false,
      asNavFor: '.small-slider'
    });
  }
  if ($('.small-slider').length) {
    $('.small-slider').slick({
     
      asNavFor: '.slider-cont',
      dots: false,
      centerMode: false,
      rtl: document.dir === "rtl" ? true : false,
      focusOnSelect: true,
      slidesToShow: 6,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1530,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            
          }
        },
        {
          breakpoint: 1333,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            
          }
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 555,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
        
      ],
      prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>'
    });
  }

  // $(".parteners-slider-top").each(function () {
  //   $(this).slick({
  //     slidesToShow: 1,
  //     slidesToScroll: 1,
  //     arrows: false,
  //     rtl: document.dir === "rtl" ? true : false,
  //     fade: true,
  //     asNavFor: '.parteners-slider-down'
  //   })
  // });
  // $(".parteners-slider-down").each(function () {
  //   $(this).slick({
  //     slidesToShow: 6,
  //     slidesToScroll: 1,
  //     centerPadding: "50px",
  //     asNavFor: '.parteners-slider-top',
  //     dots: false,
  //     centerMode: true,
  //     rtl: document.dir === "rtl" ? true : false,
  //     focusOnSelect: true,
  //     prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
  //     nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>'
  //   })
  // });
  // Slick in multiple tabs
  // $('.tab').on('tablinks', function (e) {
  //   $('.slick-slider').slick('setPosition');
  // });

  $(".about-succes_carousel").each(function () {
    $(this).slick({
      // variableWidth: true,
      infinite: true,
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerPadding: "300px",
      arrows: true,
      dots: false,
      prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            centerPadding: "200px"
            
          }
        },
        {
          breakpoint: 991,
          settings: {
            centerPadding: "100px"
            
          }
        },
        {
          breakpoint: 700,
          settings: {
            centerPadding: "50px"
            
          }
        },
        {
          breakpoint: 500,
          settings: {
            centerPadding: "22px"
            
          }
        }
        
      ],
      rtl: document.dir === "rtl" ? true : false,
      // adaptiveHeight: true
    })
  });
  $(".mediaslider").each(function () {
    $(this).slick({
      // variableWidth: true,
      infinite: true,
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerPadding: "300px",
      arrows: false,
      dots: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            centerPadding: "200px"
            
          }
        },
        {
          breakpoint: 991,
          settings: {
            centerPadding: "100px"
            
          }
        },
        {
          breakpoint: 700,
          settings: {
            centerPadding: "50px"
            
          }
        },
        {
          breakpoint: 500,
          settings: {
            centerPadding: "22px"
            
          }
        }
        
      ],
      rtl: document.dir === "rtl" ? true : false,
      // adaptiveHeight: true
    })
  });
 

  if ($(".about-succes_carousel").length > 0) {
    
    $(".about-succes_carousel  .about-success_more").each(function (){

      if($(this).prev().height() <= 60){
        $(this).hide();
      }
    });
    
    $(".about-success_more").click(function () {
      
      $(this).parent().find(".about-success_content").toggleClass('active');
      ;
      if($(this).parent().find(".about-success_content").hasClass('active')){
        if(document.dir == "rtl"){
          $(this).html('<span>اقل</span><i class="fas fa-chevron-up"></i>');
        }else{
          $(this).html('<span>Less</span><i class="fas fa-chevron-up"></i>');
        }
      }else{
        if(document.dir == "rtl"){
          $(this).html('<span>المزيد</span><i class="fas fa-chevron-down"></i>');
        }else{
          $(this).html('<span>Read more</span><i class="fas fa-chevron-down"></i>');
        }
      }
      
      
    })
    $(".about-success_more").click(function () {
      
      $(this).parent().find(".about-success_content").toggleClass('active');
      ;
      if($(this).parent().find(".about-success_content").hasClass('active')){
        if(document.dir == "rtl"){
          $(this).html('<span>اقل</span><i class="fas fa-chevron-up"></i>');
        }else{
          $(this).html('<span>Less</span><i class="fas fa-chevron-up"></i>');
        }
      }else{
        if(document.dir == "rtl"){
          $(this).html('<span>المزيد</span><i class="fas fa-chevron-down"></i>');
        }else{
          $(this).html('<span>Read more</span><i class="fas fa-chevron-down"></i>');
        }
      }
      
      
    })
    

    $(".about-charity_more").click(function () {
      
      $(this).prev().toggleClass('active');
      ;
      if($(this).prev().hasClass('active')){
        if(document.dir == "rtl"){
          $(this).html('<span>اقل</span><i class="fas fa-chevron-up"></i>');
        }else{
          $(this).html('<span>Less</span><i class="fas fa-chevron-up"></i>');
        }
      }else{
        if(document.dir == "rtl"){
          $(this).html('<span>المزيد</span><i class="fas fa-chevron-down"></i>');
        }else{
          $(this).html('<span>Read more</span><i class="fas fa-chevron-down"></i>');
        }
      }
      
      
    })
  }
  // if ($(".parteners-slider-top").length > 0) {
  //   $(".parteners-more").click(function () {
  //     $(this).parent().find(".parteners-content").css({
  //       height: "auto"
  //     })
  //     $(this).hide();
  //   })
  // }
  // $("#defaultOpen").click();
});

// function openCity(evt, cityName) {
//   // Declare all variables
//   var i, tabcontent, tablinks;

//   // Get all elements with class="tabcontent" and hide them
//   tabcontent = document.getElementsByClassName("tabcontent");
//   for (i = 0; i < tabcontent.length; i++) {
//     tabcontent[i].style.display = "none";
//   }

//   // Get all elements with class="tablinks" and remove the class "active"
//   tablinks = document.getElementsByClassName("tablinks");
//   for (i = 0; i < tablinks.length; i++) {
//     tablinks[i].className = tablinks[i].className.replace(" active", "");
//   }

//   // Show the current tab, and add an "active" class to the button that opened the tab
//   document.getElementById(cityName).style.display = "block";
//   evt.currentTarget.className += " active";
// }


// var currencyInput = document.querySelector('');

// currencyInput.addEventListener('focus', onFocus);
// currencyInput.addEventListener('blur', onBlur);


$(document).ready(function () {
  let tabLink = $('.tablinks'),
  tabContent = $('.tabcontent'),
  detailsPanel = $('.parteners-slider-top');

tabLink.first().addClass('active');
tabContent.hide().first().show();
detailsPanel.hide().first().show();
$('#partners-slider-0').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  centerPadding: "50px",
  dots: false,
  centerMode: true,
  rtl: document.dir === "rtl",
  focusOnSelect: true,
  prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
  nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 5,
        
      }
    }, {
      breakpoint: 767,
      settings: {
        slidesToShow: 5,
        // centerPadding: "150px",
        
      }
    }
    , {
      breakpoint: 550,
      settings: {
        slidesToShow: 5,
        centerPadding: "0px",
        
      }
    }
  ]
});
tabLink.click(function () {
  tabContent.hide();
  tabLink.removeClass('active');
  $(this).addClass('active');
  $(`#tab-${$(this).data('tab')}`).show();
  let sliderId = `#partners-slider-${$(this).data('tab')}`;
  let partialId = "details-" + $(this).data('tab');
  $(`div[id^=${partialId}]`).first().show();
  $(sliderId).slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      centerPadding: "50px",
      dots: false,
      centerMode: false,
      rtl: document.dir === "rtl",
      focusOnSelect: true,
      prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
      nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            
          }
        }, {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            centerPadding: "150px",
            
          }
        }
        , {
          breakpoint: 550,
          settings: {
            slidesToShow: 1,
            centerPadding: "0px",
            
          }
        }
      ]
  });
});
$('.partner-img-select').click(function () {
  $('.parteners-slider-top').hide();
  let detailsId = `#details-${$(this).data('partner')}`;
  $(detailsId).show()
});
if ($(".parteners-slider-top").length > 0) {
  // $(".parteners-more").click(function () {
  //     $(this).parent().find(".parteners-content").css({
  //         height: "auto"
  //     })
  //     $(this).hide();
  // })

    
    // $(".parteners  .parteners-more").each(function (){
    //   console.log($(this).prev().height());
    //   if($(this).prev().height() <= 160){
    //     $(this).hide();
    //   }
    // });
    
    $(".parteners  .parteners-more").click(function () {
      
      $(this).prev().toggleClass('active');
      ;
      if($(this).prev().hasClass('active')){
        if(document.dir == "rtl"){
          $(this).html('<i class="fas fa-chevron-up"></i><span>اقل</span>');
        }else{
          $(this).html('<i class="fas fa-chevron-up"></i><span>Less</span>');
        }
      }else{
        if(document.dir == "rtl"){
          $(this).html('<span>المزيد</span><i class="fas fa-chevron-down"></i>');
        }else{
          $(this).html('<span>Read more</span><i class="fas fa-chevron-down"></i>');
        }
      }
      
      
    })
  
}
})
$(window).on('load', function () {
  setTimeout(function () {
    $('.pop-wrapper').fadeOut();
  }, 5000);
})
$(document)
  .on('change', '.contact-type-radio input[type=radio]', function () {
    $('.contactTypeLabel').hide();
    $('.' + $('.contact-type-radio input:radio:checked').attr('id')).show();
  })
  .on('change', '#messageCity', function () {
    $('.locations-container > div').hide();
    $('#' + $(this).val()).show();
  })
  .on('click', '.map-list .dropdown > a', function (e) {
    e.preventDefault();
    $(this).parent().toggleClass('open');
    $(this).parent().find('ul').slideToggle();
  })
  .on('click', '.calculator-page .cart-total_continue:not(.donition-fields .cart-total_continue)', function (e) {
    e.preventDefault();
    $(this).hide();
    $('.donition-fields').show();
  })
  .on('click', '.join-btn', function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $('.join-container').offset().top - 150
    })
  })
  .on('click', '.remove-card', function (e) {
    e.preventDefault();
    let parent = $(this).parents().closest('.card-item')
    parent.fadeOut('slow', function () {
      parent.remove();
    })
  })
  .on('click', '.quick-donation-btn', function () {
    $('.quick-donate-form').show();
  })
  .on('click', '.quick-donate-cont', function () {
    $('.quick-donate-form').hide();
    $('.quick-donation-fields').show();
  })
  $(document).ready(function () {
    if($( window ).width() <= 767){
      $('.footer-list .footer-list_title').attr('data-toggle','collapse');
      $('.footer-list .footer-list_title').addClass('collapsed');
      $('.footer-list ul.submenu').addClass('collapse ');
    }
  });
  $(document).ready(function () {
    $('.cart-form_body .form-group').each( function (){
      // $(this).find('label').height();
      $(this).css("padding-top",$(this).find('label').height())
    });
    $('.cart-form-collapse').click(function (){
      $('.contact-collapse').addClass('active');
      $(this).hide();
      $('.cart-form-collapse-up').show();
    });
    $('.cart-form-collapse-up').click(function (){
      $('.contact-collapse').removeClass('active');
      $(this).hide();
      $('.cart-form-collapse').show();
    });


    // map
    $('.open-map-list').click(function (){
      $('.map-list').css('display','block');
      $('.cart-list_total').css('display','none');
    });
    $('.close-map-list').click(function (){
      $('.map-list').css('display','none');
    });
    $('.close-cart-list_total').click(function (){
      $('.cart-list_total').css('display','none');
    });
    
  });
  $(".project_more").click(function () {
    $(this).parent().find(".article-group").toggleClass('active');
    ;
    if($(this).parent().find(".article-group").hasClass('active')){
      if(document.dir == "rtl"){
        $(this).html('<span>اقل</span><i class="fas fa-chevron-up"></i>');
      }else{
        $(this).html('<span>Less</span><i class="fas fa-chevron-up"></i>');
      }
    }else{
      if(document.dir == "rtl"){
        $(this).html('<span>المزيد</span><i class="fas fa-chevron-down"></i>');
      }else{
        $(this).html('<span>Read more</span><i class="fas fa-chevron-down"></i>');
      }
    }
    
    
  })