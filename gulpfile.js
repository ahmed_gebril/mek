const gulp = require('gulp')
const postcss = require('gulp-postcss')
const sass = require('gulp-sass')
const moment = require('moment')
const autoprefixer = require('autoprefixer')
const log = require('fancy-log')
const size = require('gulp-size')
const plumber = require('gulp-plumber')
const sourceMaps = require('gulp-sourcemaps')
const header = require('gulp-header')
const cssnano = require('cssnano')
const browserSync = require('browser-sync').create()
const pkg = require('./package.json')
const mqpacker = require('css-mqpacker')
const htmlhint = require('gulp-htmlhint')
const changed = require('gulp-changed')
const imagemin = require('gulp-imagemin')
const rucksack = require('rucksack-css')
// const rtlcss = require('gulp-rtlcss')
const rtlcss = require('postcss-rtlcss-combined')
const rename = require('gulp-rename');
const concatCss = require('gulp-concat-css');

sass.compiler = require('node-sass');

// Log Errors Function
const onError = (err) => {
  console.log(err);
};

const banner = [
  "/**",
  " * @Project        x",
  " * @Author         x",
  " * @Job Title      x",
  " * @Build          " + moment().format("llll") + " ET",
  " **/",
  ""
].join("\n");

const processors = [
  autoprefixer(["last 2 versions", "> 1%"]),
  mqpacker,
  rucksack({
    fallbacks: true
  }),
  rtlcss()
];

// HTML Tasks
gulp.task('html', () => {
  log('=== Compiling HTML ===')
  return gulp.src('src/*.html')
    .pipe(changed('dist/'))
    .pipe(htmlhint())
    .pipe(htmlhint.reporter())
    .pipe(size())
    .pipe(gulp.dest('build/'))
    .pipe(gulp.dest('dist/'))
    .pipe(browserSync.reload({
      stream: true
    }));
})

// Stylesheets Tasks
gulp.task('cssBuild', () => {
  log('=== Compiling Sass ===')
  return gulp.src('src/sass/**/*.+(scss|sass)')
    .pipe(changed('build/css/'))
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(size())
    .pipe(sourceMaps.init({
      loadMaps: true
    }))
    // .pipe(wait(50))
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(gulp.dest('build/css/'))
    // .pipe(rtlcss())
    // .pipe(rename({ suffix: '-rtl' }))
    // .pipe(concatCss("all.css"))
    // .pipe(sourceMaps.write('./'))
    // .pipe(gulp.dest('build/css/'))
})
gulp.task('cssDist', ['cssBuild'], () => {
  log('=== Compiling Distributing CSS ===')
  return gulp.src('build/css/*.css')
    .pipe(changed('dist/css/'))
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourceMaps.init({
      loadMaps: true
    }))
    .pipe(postcss([cssnano({
      reduceIdents: false,
      zindex: false,
      discardUnused: {
        fontFace: false
      }
    })]))
    .pipe(header(banner, {
      pkg: pkg
    }))
    .pipe(sourceMaps.write('./'))
    .pipe(size())
    .pipe(gulp.dest('dist/css/'))
    .pipe(browserSync.reload({
      stream: true
    }))
})

// Javascript Tasks
gulp.task('js', () => {
  log('=== Compiling Javascript via Babel ===')
  return gulp.src('src/js/**/*.js')
    .pipe(plumber({
      errorHandler: onError
    }))
    // .pipe(babel({
    //   presets: ['env']
    // }))
    .pipe(size())
    .pipe(gulp.dest('build/js/'))
    // .pipe(uglify())
    .pipe(header(banner, {
      pkg: pkg
    }))
    .pipe(size())
    .pipe(gulp.dest('dist/js/'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// Image Optimization Tasks
gulp.task('img', function () {
  log('=== Moving Images ===')
  return gulp.src('src/images/**/*.+(png|jpg|gif|svg)')
    .pipe(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 5
    }))
    .pipe(gulp.dest('dist/images/'))
    .pipe(gulp.dest('build/images/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});


// Watching Function

gulp.task('watch', () => {
  gulp.watch('src/*.html', ['html'])
  gulp.watch('src/sass/**/*.+(scss|sass)', ['cssDist'])
  gulp.watch('src/js/**/*.js', ['js'])
  gulp.watch('src/img/**/*.+(png|jpg|gif|svg)', ['img'])
  // gulp.watch('src/fonts/**/*.+(ttf|woff|eof|svg|otf|eot|woff2)', ['fonts'])
})


gulp.task('browserSync', () => {
  browserSync.init({
    watch: true,
    server: {
      baseDir: 'build/'
    },
  })
});

gulp.task('default', ['html','cssDist', 'img', 'js', 'browserSync', 'watch'])